package net.minecraft.src;

public class GuiTwoLeanFour extends GuiScreen {
	
	public void initGui()
	{
		//controls go here
		
		
		controlList.clear();
		
	    // more buttons
		controlList.add(new GuiButton(1, width / 2, height / 2 - 120, 100, 20, "Mods"));
		controlList.add(new GuiButton(3, width / 2 - 100, height / 2 - 120, 100, 20, "Toggle Menu"));
		controlList.add(new GuiButton(2, width / 2, height / 2 + 20, 100, 20, "Exit"));
		controlList.add(new GuiButton(4, width / 2 - 100, height / 2 + 20, 100, 20, "Back"));
		controlList.add(new GuiButton(5, width / 2 - 100, height / 2 - 100, 100, 20, "Last Menu Page"));
		controlList.add(new GuiButton(6, width / 2, height / 2 - 100, 100, 20, "Next Menu Page"));
	}
	
	 protected void actionPerformed(GuiButton par1GuiButton)
	    {
		 	
		 	if(par1GuiButton.id == 2)
		 	{
		 		mc.displayGuiScreen(null);
		 	}
		 	
		 	if(par1GuiButton.id == 3)
		 	{
		 		Hacks.menu = !Hacks.menu;
		 	}
		 	if(par1GuiButton.id == 1)
		 	{
		 		mc.displayGuiScreen(new GuiHacks());
		 	}
		 	
		 	if(par1GuiButton.id == 4)
		 	{
		 		mc.displayGuiScreen(new GuiIngameMenu());
		 	}
		 	if(Hacks.menu){
		 	if(par1GuiButton.id == 5)
		 	{
		 		Hacks.menupage = Hacks.menupage - 1;
		 	}
		 	
		 	if(par1GuiButton.id == 6)
		 	{
		 		Hacks.menupage = Hacks.menupage + 1;
		 	}}
		 
	    }
	public void drawScreen(int par1, int par2, float par3)
	{
		//text goes above the draw screen
		
		
		
		super.drawScreen(par1, par2, par3);
	}

}
