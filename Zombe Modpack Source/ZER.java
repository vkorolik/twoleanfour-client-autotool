package net.minecraft.src;

import net.minecraft.client.Minecraft;

public final class ZER extends EntityRenderer {

    public ZER(Minecraft minecraft, EntityRenderer prev) {
        super(minecraft);
        fogColorBuffer = prev.fogColorBuffer;
        itemRenderer = prev.itemRenderer;
        lightmapTexture = prev.lightmapTexture;
    }

    public void updateCameraAndRender(float par) {
        super.updateCameraAndRender(par);
        ZMod.pingDrawGUIHandle(par);
    }
    
    protected void renderRainSnow(float par) {
        ZMod.spoofCloudFogConfig();
        if(ZMod.drawRainHandle()) super.renderRainSnow(par);
    }
    
/* too god-damn broken
    private static EntityPlayerSP voodooDoll;
    private static void setVoodooDoll() {
        Minecraft mc = ZMod.getMinecraft();
        if(voodooDoll == null) voodooDoll = new EntityPlayerSP(mc, mc.theWorld, mc.k, 0); // session: % 1000L  * xx.session
        EntityLiving view = mc.renderViewEntity;
        voodooDoll.prevPosX = voodooDoll.lastTickPosX = voodooDoll.posX = view.posX;
        voodooDoll.prevPosY = voodooDoll.lastTickPosY = voodooDoll.posY = view.posY + view.getMountedYOffset();
        voodooDoll.prevPosZ = voodooDoll.lastTickPosZ = voodooDoll.posZ = view.posZ;
        voodooDoll.u = voodooDoll.rotationYaw = view.rotationYaw;
        voodooDoll.v = voodooDoll.rotationPitch = view.rotationPitch;
        mc.renderViewEntity = voodooDoll;
    }
    
    public void a(float par1, long par2) { // 0x3b9aca00L  * is in function
        Minecraft mc = ZMod.getMinecraft();
        EntityLiving view = mc.renderViewEntity;
        if(!(mc.renderViewEntity instanceof EntityPlayer)) setVoodooDoll();
        super.a(par1, par2);
        mc.renderViewEntity = view;
    }
*/

    public void updateRenderer() {
        Minecraft mc = ZMod.getMinecraft();
        EntityLiving view = mc.renderViewEntity;
        if(!(mc.renderViewEntity instanceof EntityPlayerSP)) mc.renderViewEntity = mc.thePlayer; // setVoodooDoll();
        
        super.updateRenderer();

        mc.renderViewEntity = view;
    }

}
